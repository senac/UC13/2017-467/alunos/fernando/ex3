/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author sala304b
 */
public class ConverterMoeda {
    
    private static final double COTACAO_DOLLAR = 4.08;
    private static final double COTACAO_EURO = 4.80;
    private static final double COTACAO_DOLLAR_AUSTRALIANO = 2.95;

    public double converterParaDollar(double valorReal) {
        return valorReal/COTACAO_DOLLAR ;
    }

    public double converterParaEuro(double valorReal) {
        return valorReal/COTACAO_EURO;
    }

    public double converterParaDollarAustraliano(double valorReal) {
        return valorReal/COTACAO_DOLLAR_AUSTRALIANO;
    }
    
}

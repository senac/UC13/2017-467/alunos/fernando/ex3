/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.ConverterMoeda;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class ConverterTest {
    
    public ConverterTest() {
    }
    
    @Test
    public void deveConverter10ReaisParaDollar(){
        ConverterMoeda converterMoeda = new ConverterMoeda();
        double valorReal = 10;
        double resultado = converterMoeda.converterParaDollar(valorReal);
        
        assertEquals(2.45, resultado, 0.05);
    }
    
    @Test
    public void deveConverter10ReaisParaEuro(){
        ConverterMoeda converterMoeda = new ConverterMoeda();
        double valorReal = 10;
        double resultado = converterMoeda.converterParaEuro(valorReal);
        
        assertEquals(2.08, resultado, 0.05);
    }
    
    @Test
    public void deveConverter10ReaisParaDollarAustraliano(){
        ConverterMoeda converterMoeda = new ConverterMoeda();
        double valorReal = 10;
        double resultado = converterMoeda.converterParaDollarAustraliano(valorReal);
        
        assertEquals(3.38, resultado, 0.05);
    }
    
}
